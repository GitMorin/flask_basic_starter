from flask import Flask

app = Flask(__name__) # Create flask app

from app import views
from app import admin_views