from app import app # importing from app from the package app

@app.route("/")
def index():
  return "Hello world!"

@app.route("/about")
def about():
  return "<h1>About us</h1>"